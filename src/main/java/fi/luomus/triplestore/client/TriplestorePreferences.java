package fi.luomus.triplestore.client;

import java.net.URI;

public interface TriplestorePreferences {
    URI getBaseUri();
    String getUsername();
    String getPassword();
}
