package fi.luomus.triplestore.client;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.jena.rdf.model.Model;

import fi.luomus.utils.exceptions.ApiException;

public abstract class AbstractSearchQuery implements TriplestoreClient.SearchQuery {
	protected String subjectClass;
	protected String predicate;
	protected Collection<String> objects = new ArrayList<>();
	protected final String objectType;
	protected Integer limit;
	protected Integer offset;

	public AbstractSearchQuery(String objectType) {
		this.objectType = objectType;
	}

	@Override
	public TriplestoreClient.SearchQuery subjectClass(String subjectClassQname) {
		this.subjectClass = subjectClassQname;
		return this;
	}

	@Override
	public TriplestoreClient.SearchQuery predicate(String predicateQname) {
		this.predicate = predicateQname;
		return this;
	}

	@Override
	public TriplestoreClient.SearchQuery object(String objectQnameOrLiteral) {
		this.objects.add(objectQnameOrLiteral);
		return this;
	}

	@Override
	public TriplestoreClient.SearchQuery object(Collection<String> objectQnamesOrLiterals) {
		this.objects.addAll(objectQnamesOrLiterals);
		return this;
	}

	@Override
	public TriplestoreClient.SearchQuery object(String... objectQnamesOrLiterals) {
		for (String object : objectQnamesOrLiterals) {
			this.objects.add(object);
		}
		return this;
	}

	@Override
	public TriplestoreClient.SearchQuery limit(int limit) {
		if (limit <= 0) {
			throw new IllegalArgumentException("limit must be greater than zero");
		}
		this.limit = limit;
		return this;
	}

	@Override
	public TriplestoreClient.SearchQuery offset(int offset) {
		if (offset < 0) {
			throw new IllegalArgumentException("offset must not be negative");
		}
		this.offset = offset;
		return this;
	}

	@Override
	public Model execute() throws ApiException {
		if (predicate == null || limit == null || offset == null) {
			throw new IllegalStateException("Missing required parameter");
		}
		return actualExecute();
	}

	protected abstract Model actualExecute() throws ApiException;
}
