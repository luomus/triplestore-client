package fi.luomus.triplestore.client;

import java.util.HashMap;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;

public class MultiModelParser {

	private MultiModelParser() {}

	public static Map<String, Resource> parseModel(Model model) {
		HashMap<String, Resource> result = new HashMap<>();
		ResIterator resIterator = model.listSubjects();
		while (resIterator.hasNext()) {
			Resource next = resIterator.next();
			result.put(next.getLocalName(), next);
		}
		return result;
	}

}
