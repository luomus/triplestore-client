package fi.luomus.triplestore.client.model;

public class TaxonSearchMatch {
    private String taxonId;
    private String content;
    private String scientificName;
    private String scientificNameAuthorship;
    private String taxonRank;

    public String getTaxonId() {
        return taxonId;
    }

    public void setTaxonId(String taxonId) {
        this.taxonId = taxonId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getScientificName() {
        return scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public String getScientificNameAuthorship() {
        return scientificNameAuthorship;
    }

    public void setScientificNameAuthorship(String scientificNameAuthorship) {
        this.scientificNameAuthorship = scientificNameAuthorship;
    }

    public String getTaxonRank() {
        return taxonRank;
    }

    public void setTaxonRank(String taxonRank) {
        this.taxonRank = taxonRank;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaxonSearchMatch that = (TaxonSearchMatch) o;

        if (taxonId != null ? !taxonId.equals(that.taxonId) : that.taxonId != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (scientificName != null ? !scientificName.equals(that.scientificName) : that.scientificName != null)
            return false;
        if (scientificNameAuthorship != null ? !scientificNameAuthorship.equals(that.scientificNameAuthorship) : that.scientificNameAuthorship != null)
            return false;
        return !(taxonRank != null ? !taxonRank.equals(that.taxonRank) : that.taxonRank != null);

    }

    @Override
    public int hashCode() {
        int result = taxonId != null ? taxonId.hashCode() : 0;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (scientificName != null ? scientificName.hashCode() : 0);
        result = 31 * result + (scientificNameAuthorship != null ? scientificNameAuthorship.hashCode() : 0);
        result = 31 * result + (taxonRank != null ? taxonRank.hashCode() : 0);
        return result;
    }
}
