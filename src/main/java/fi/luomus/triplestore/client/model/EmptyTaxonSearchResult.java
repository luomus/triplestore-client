package fi.luomus.triplestore.client.model;

class EmptyTaxonSearchResult {
    private String results;

    public String getResults() {
        return results;
    }

    public void setResults(String results) {
        this.results = results;
    }
}
