package fi.luomus.triplestore.client.model;

import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;

/**
 * Various constants useful for parsing Jena models
 */
public final class Constants {

	private Constants() {}

	public final static String DEFAULT_NS_URI = "http://tun.fi/";
	public final static String DWC_NS_URI = "http://rs.tdwg.org/dwc/terms/";
	public final static String RDFS_NS_URI = "http://www.w3.org/2000/01/rdf-schema#";
	public final static String RDF_NS_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public final static Property TAXON_RANK = ModelFactory.createDefaultModel().createProperty(DEFAULT_NS_URI, "MX.taxonRank");
	public final static Property SCIENTIFIC_NAME = ModelFactory.createDefaultModel().createProperty(DEFAULT_NS_URI, "MX.scientificName");
	public final static Property VERNACULAR_NAME = ModelFactory.createDefaultModel().createProperty(DEFAULT_NS_URI, "MX.vernacularName");
	public final static Property LABEL = ModelFactory.createDefaultModel().createProperty(RDFS_NS_URI, "label");

}
