package fi.luomus.triplestore.client.model;

import java.util.ArrayList;
import java.util.List;

public class TaxonSearchResult {
    private List<TaxonSearchMatch> exactMatch = new ArrayList<>();
    private List<TaxonSearchMatchWithSimilarity> likelyMatches = new ArrayList<>();
    private List<TaxonSearchMatch> partialMatches = new ArrayList<>();


    public List<TaxonSearchMatch> getExactMatch() {
        return exactMatch;
    }

    public void setExactMatch(List<TaxonSearchMatch> exactMatch) {
        this.exactMatch = exactMatch;
    }

    public List<TaxonSearchMatchWithSimilarity> getLikelyMatches() {
        return likelyMatches;
    }

    public void setLikelyMatches(List<TaxonSearchMatchWithSimilarity> likelyMatches) {
        this.likelyMatches = likelyMatches;
    }

    public List<TaxonSearchMatch> getPartialMatches() {
        return partialMatches;
    }

    public void setPartialMatches(List<TaxonSearchMatch> partialMatches) {
        this.partialMatches = partialMatches;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaxonSearchResult that = (TaxonSearchResult) o;

        if (exactMatch != null ? !exactMatch.equals(that.exactMatch) : that.exactMatch != null) return false;
        if (likelyMatches != null ? !likelyMatches.equals(that.likelyMatches) : that.likelyMatches != null)
            return false;
        return !(partialMatches != null ? !partialMatches.equals(that.partialMatches) : that.partialMatches != null);

    }

    @Override
    public int hashCode() {
        int result = exactMatch != null ? exactMatch.hashCode() : 0;
        result = 31 * result + (likelyMatches != null ? likelyMatches.hashCode() : 0);
        result = 31 * result + (partialMatches != null ? partialMatches.hashCode() : 0);
        return result;
    }
}
