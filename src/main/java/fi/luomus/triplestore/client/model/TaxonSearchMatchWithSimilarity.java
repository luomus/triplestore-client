package fi.luomus.triplestore.client.model;

public class TaxonSearchMatchWithSimilarity extends TaxonSearchMatch {
    private Float similarity;

    public Float getSimilarity() {
        return similarity;
    }

    public void setSimilarity(Float similarity) {
        this.similarity = similarity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TaxonSearchMatchWithSimilarity that = (TaxonSearchMatchWithSimilarity) o;

        return !(similarity != null ? !similarity.equals(that.similarity) : that.similarity != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (similarity != null ? similarity.hashCode() : 0);
        return result;
    }
}
