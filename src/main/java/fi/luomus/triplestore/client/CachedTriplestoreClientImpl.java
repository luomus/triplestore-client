package fi.luomus.triplestore.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import com.google.common.base.Optional;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.UncheckedExecutionException;

import fi.luomus.triplestore.client.model.TaxonSearchResult;
import fi.luomus.triplestore.client.util.PredicateUpdateOperation;
import fi.luomus.utils.exceptions.ApiException;

/**
 * Cached wrapper triplestore client that requires a working implementation of Triplestore client
 * to execute actual queries. Invalidates caches based on update operations
 */
public class CachedTriplestoreClientImpl implements TriplestoreClient {
	private final static Map<String, String> NO_PARAMS = new HashMap<>();
	private final LoadingCache<CacheKey, Model> modelCache;
	private final LoadingCache<List<String>, Map<String, Resource>> multiResourceCache;
	private final TriplestoreClient source;
	public static final int DEFAULT_CACHE_DURATION = 60;
	public static final int DEFAULT_MAXIMUM_CACHE_SIZE = 500;


	@Inject
	public CachedTriplestoreClientImpl(TriplestoreClient source) {
		this(DEFAULT_CACHE_DURATION, source);
	}

	@Inject
	public CachedTriplestoreClientImpl(int minutes, TriplestoreClient source) {
		this(minutes, DEFAULT_MAXIMUM_CACHE_SIZE, source);
	}

	@Inject
	public CachedTriplestoreClientImpl(int minutes, int maximumCacheSize, TriplestoreClient source) {
		if (minutes <= 0) {
			throw new IllegalStateException("invalid cache duration");
		}
		this.source = source;
		final CachedTriplestoreClientImpl thiz = this;

		this.modelCache = CacheBuilder.newBuilder()
				.expireAfterWrite(minutes, TimeUnit.MINUTES)
				.maximumSize(maximumCacheSize)
				.build(new CacheLoader<CacheKey, Model>() {
					@Override
					public Model load(CacheKey cacheKey) throws Exception {
						Optional<Model> got = thiz.source.getResource(cacheKey.resourceName, cacheKey.params);
						if (!got.isPresent()) {
							// prevent caching of absent values
							throw new NotFoundException();
						}
						return got.get();
					}
				});

		multiResourceCache = CacheBuilder.newBuilder()
				.expireAfterWrite(minutes, TimeUnit.MINUTES)
				.maximumSize(maximumCacheSize)
				.build(new CacheLoader<List<String>, Map<String, Resource>>() {
					@Override
					public Map<String, Resource> load(List<String> key) throws Exception {
						return thiz.source.getResources(key);

					}
				});
	}

	@Override
	public SearchQuery searchResource() {
		return this.source.searchResource();
	}

	@Override
	public SearchQuery searchLiteral() {
		return this.source.searchLiteral();
	}

	@Override
	public String getSequence(String namespace) throws ApiException {
		return this.source.getSequence(namespace);
	}

	@Override
	public Optional<Model> getResource(String resourceName) throws ApiException {
		try {
			return this.getResource(resourceName, NO_PARAMS);
		} catch (TooManyResultsException e) {
			// this shouldnt happen without params
			throw new RuntimeException(e);
		}
	}

	@Override
	public Optional<Model> getResource(String resourceName, Map<String, String> params) throws ApiException, TooManyResultsException {
		try {
			return Optional.fromNullable(modelCache.get(new CacheKey(resourceName, params)));
		} catch (ExecutionException e) {
			if (e.getCause() instanceof ApiException) {
				throw (ApiException) e.getCause();
			} else if (e.getCause() instanceof TooManyResultsException) {
				throw (TooManyResultsException) e.getCause();
			} else {
				throw new RuntimeException(e);
			}
		} catch (UncheckedExecutionException e) {
			if (e.getCause() instanceof NotFoundException) {
				return Optional.absent();
			}
			throw (e);
		}
	}

	@Override
	public void upsertResource(String resourceName, Model model) throws ApiException {
		this.source.upsertResource(resourceName, model);
		invalidateResource(resourceName);
	}

	@Override
	public void delete(String resourceName) throws ApiException {
		this.source.delete(resourceName);
		invalidateResource(resourceName);
	}

	@Override
	public TaxonSearchResult taxonSearch(String query) throws ApiException {
		return this.source.taxonSearch(query);
	}

	@Override
	public TaxonSearchResult taxonSearch(String query, String checklist) throws ApiException {
		return this.source.taxonSearch(query, checklist);
	}

	@Override
	public void updatePredicate(PredicateUpdateOperation operation) throws ApiException {
		this.source.updatePredicate(operation);
		invalidateResource(operation.resourceName);
	}

	@Override
	public Map<String, Resource> getResources(String... resourceNames) throws ApiException {
		return null;
	}

	@Override
	public Map<String, Resource> getResources(List<String> resourceNames) throws ApiException {
		return null;
	}

	private void invalidateResource(String resourceName) {
		// we have to do this since the same resource name might be
		// cached with different parameters
		for (CacheKey key : this.modelCache.asMap().keySet()) {
			if (key.resourceName.equals(resourceName)) {
				this.modelCache.invalidate(key);
			}
		}
		for (List<String> keys : this.multiResourceCache.asMap().keySet()) {
			if (keys.contains(resourceName)) {
				this.multiResourceCache.invalidate(keys);
			}
		}
	}

	private final static class CacheKey {
		public final String resourceName;
		public final Map<String, String> params;

		private CacheKey(String resourceName, Map<String, String> params) {
			this.resourceName = resourceName;
			this.params = params;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			CacheKey cacheKey = (CacheKey) o;

			if (params != null ? !params.equals(cacheKey.params) : cacheKey.params != null) return false;
			if (resourceName != null ? !resourceName.equals(cacheKey.resourceName) : cacheKey.resourceName != null)
				return false;

			return true;
		}

		@Override
		public int hashCode() {
			int result = resourceName != null ? resourceName.hashCode() : 0;
			result = 31 * result + (params != null ? params.hashCode() : 0);
			return result;
		}
	}

	private final static class NotFoundException extends RuntimeException {
		private static final long serialVersionUID = 309990032270206678L;
	}
}
