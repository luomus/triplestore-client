package fi.luomus.triplestore.client;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import com.google.common.base.Optional;

import fi.luomus.triplestore.client.model.TaxonSearchResult;
import fi.luomus.triplestore.client.util.PredicateUpdateOperation;
import fi.luomus.utils.exceptions.ApiException;

public interface TriplestoreClient {

	String SOURCE = "TRIPLESTORE";
	int DEFAULT_LIMIT = 1000;

	/**
	 * Search for object
	 *
	 * @return Search query builder
	 */
	SearchQuery searchResource();

	/**
	 * Search for literal
	 *
	 * @return Search query builder
	 */
	SearchQuery searchLiteral();

	/**
	 * Retrieve and implicitly reserve the next available sequence in the given namespace
	 *
	 * @param namespace Name of the namespace
	 * @return The next avaialable sequence
	 * @throws ApiException
	 */
	String getSequence(String namespace) throws ApiException;

	/**
	 * Retrieve the resource corresponding to the given resource name with no extra parameters
	 *
	 * @param resourceName Name of the resource (e.g. "MX.123")
	 * @return Optional of Jena model of the resource if resource exists, absent otherwise
	 * @throws ApiException
	 */
	Optional<Model> getResource(String resourceName) throws ApiException;

	Map<String, Resource> getResources(String... resourceNames) throws ApiException;
	Map<String, Resource> getResources(List<String> resourceNames) throws ApiException;

	/**
	 * Delete the resource that corresponds to the given resource name
	 *
	 * @param resourceName Name of the resource
	 * @throws ApiException
	 */
	void delete(String resourceName) throws ApiException;

	/**
	 * Retrieve the resourse corresponding to the given resource name with given custom query parameters
	 * @param resourceName Name of the resource
	 * @param params Custom query parameters
	 * @return Optional of Jena model of the resource if resource exists, absent otherwise
	 * @throws ApiException
	 * @throws TooManyResultsException Thrown if the query response is too large to handle
	 */
	Optional<Model> getResource(String resourceName, Map<String, String> params) throws ApiException, TooManyResultsException;

	/**
	 * Search for taxons matching the query string using the master checklist
	 *
	 * @param query At least 3 letters long query string
	 * @return Matches grouped by their general likelihood
	 * @throws ApiException
	 */
	TaxonSearchResult taxonSearch(String query) throws ApiException;

	/**
	 * Search for taxons matching the query string using the specified checklist
	 *
	 * @param query At least 3 letters long query string
	 * @param checklist The name of the checklist
	 * @return Matches grouped by their general likelihood
	 * @throws ApiException
	 */
	TaxonSearchResult taxonSearch(String query, String checklist) throws ApiException;

	/**
	 * Update a predicate according to the given predicate operation configuration
	 *
	 * @param operation Predicate operation configuration
	 * @throws ApiException
	 */
	void updatePredicate(PredicateUpdateOperation operation) throws ApiException;

	/**
	 * Create or update a resource with the given name using the given model
	 *
	 * @param resourceName Name of the resource
	 * @param model Jena model of the resource
	 * @throws ApiException
	 */
	void upsertResource(String resourceName, Model model) throws ApiException;

	interface SearchQuery {
		SearchQuery subjectClass(String subjectClassQname);

		SearchQuery predicate(String predicateQname);

		SearchQuery object(String objectQnameOrLiteral);

		SearchQuery object(Collection<String> objectQnamesOrLiterals);

		SearchQuery object(String... objectQnamesOrLiterals);

		SearchQuery limit(int limit);

		SearchQuery offset(int offset);

		Model execute() throws ApiException;
	}

	class TooManyResultsException extends Exception {

		private static final long serialVersionUID = 1934324727863909552L;

		public TooManyResultsException(String message, Throwable cause) {
			super(message, cause);
		}

		public TooManyResultsException(Throwable cause) {
			super(cause);
		}

		public TooManyResultsException(String message) {
			super(message);
		}
	}


}
