package fi.luomus.triplestore.client.util;


import fi.luomus.utils.model.LocalizedString;

public class PredicateUpdateOperation {
    public final String resourceName;
    public final String predicate;
    public final Long context;
    public final LocalizedString value;

    private PredicateUpdateOperation(String resource, String predicate, Long context, LocalizedString value) {
        this.resourceName = resource;
        this.predicate = predicate;
        this.context = context;
        this.value = value;
    }

    public static PredicateUpdateOperationBuilder builder() {
        return new PredicateUpdateOperationBuilder();
    }

    public static class PredicateUpdateOperationBuilder {
        private String resourceName;
        private String predicate;
        private Long context;
        private LocalizedString value;

        public PredicateUpdateOperationBuilder withResource(String resource) {
            this.resourceName = resource;
            return this;
        }

        public PredicateUpdateOperationBuilder withPredicate(String predicate) {
            this.predicate = predicate;
            return this;
        }

        public PredicateUpdateOperationBuilder withContext(Long context) {
            this.context = context;
            return this;
        }

        public PredicateUpdateOperationBuilder withValue(LocalizedString value) {
            this.value = value;
            return this;
        }

        public PredicateUpdateOperation build() {
            if (resourceName == null) {
                throw new IllegalStateException("Resource name required");
            }
            if (predicate == null) {
                throw new IllegalStateException("Predicate required");
            }
            if (value == null) {
                throw new IllegalStateException("Value required");
            }

            return new PredicateUpdateOperation(resourceName, predicate, context, value);
        }
    }
}
