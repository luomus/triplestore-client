package fi.luomus.triplestore.client.util;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

/**
 * Utility class that can be used to convert simple resources with literal predicates to a String map
 */
public final class ResourceToMapConverter {

	public static ListMultimap<String, String> getAllPredicates(Resource resource) {
		ListMultimap<String, String> map = ArrayListMultimap.create();
		StmtIterator stmtIterator = resource.listProperties();
		while (stmtIterator.hasNext()) {
			Statement next = stmtIterator.next();
			String predicateName = next.getPredicate().getLocalName();
			if (next.getObject().isLiteral()) {
				String value = next.getString();
				map.put(predicateName, value);
			}
		}
		return map;
	}

}
