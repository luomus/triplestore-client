package fi.luomus.triplestore.client.util;


import fi.luomus.triplestore.client.model.TaxonSearchMatchWithSimilarity;
import fi.luomus.triplestore.client.model.TaxonSearchResult;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class TaxonSearchResultDeserializer {
    private TaxonSearchResultDeserializer() {
    }

    public static TaxonSearchResult parse(Document source) {
        TaxonSearchResult result = new TaxonSearchResult();
        NodeList childNodes = source.getFirstChild().getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            if (item.getNodeType() == Node.ELEMENT_NODE) {
                String nodeName = item.getNodeName();
                switch (nodeName) {
                    case "exactMatch":
                        result.getExactMatch().addAll(match(item));
                        break;
                    case "likelyMatches":
                        result.getLikelyMatches().addAll(match(item));
                        break;
                    case "partialMatches":
                        result.getPartialMatches().addAll(match(item));
                        break;
                    default:
                        throw new RuntimeException("invalid node name " + nodeName);
                }
            }
        }

        return result;
    }

    private static List<TaxonSearchMatchWithSimilarity> match(Node matchType) {
        List<TaxonSearchMatchWithSimilarity> result = new ArrayList<>();
        NodeList childNodes = matchType.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node child = childNodes.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                result.add(nodeToMatch(child));
            }
        }

        return result;
    }

    private static TaxonSearchMatchWithSimilarity nodeToMatch(Node node) {
        TaxonSearchMatchWithSimilarity match = new TaxonSearchMatchWithSimilarity();
        Node similarity = node.getAttributes().getNamedItem("similarity");
        if (similarity != null) {
            match.setSimilarity(Float.valueOf(similarity.getTextContent()));
        }
        Node scientificNameAuthorship = node.getAttributes().getNamedItem("scientificNameAuthorship");
        if (scientificNameAuthorship != null) {
            match.setScientificNameAuthorship(scientificNameAuthorship.getTextContent());
        }
        Node taxonRank = node.getAttributes().getNamedItem("taxonRank");
        if (taxonRank != null) {
            match.setTaxonRank(taxonRank.getTextContent());
        }
        Node scientificName = node.getAttributes().getNamedItem("scientificName");
        if (scientificName != null) {
            match.setScientificName(scientificName.getTextContent());
        }
        match.setTaxonId(node.getNodeName());
        match.setContent(node.getAttributes().getNamedItem("matchingName").getTextContent());

        return match;
    }
}
