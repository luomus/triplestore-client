package fi.luomus.triplestore.client;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import org.w3c.dom.Document;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;

import fi.luomus.triplestore.client.model.TaxonSearchResult;
import fi.luomus.triplestore.client.util.PredicateUpdateOperation;
import fi.luomus.triplestore.client.util.TaxonSearchResultDeserializer;
import fi.luomus.utils.exceptions.ApiException;

public class HTTPTriplestoreClientImpl implements TriplestoreClient {
	private final TriplestorePreferences triplestorePreferences;
	private WebTarget target = null;

	public HTTPTriplestoreClientImpl(TriplestorePreferences triplestorePreferences) {
		this.triplestorePreferences = triplestorePreferences;
	}

	@Override
	public SearchQuery searchResource() {
		return new SearchQueryImpl("objectresource", this);
	}

	@Override
	public SearchQuery searchLiteral() {
		return new SearchQueryImpl("objectliteral", this);
	}

	private class SearchQueryImpl extends AbstractSearchQuery {
		private final HTTPTriplestoreClientImpl httpTriplestoreClient;

		private SearchQueryImpl(String searchType, HTTPTriplestoreClientImpl httpTriplestoreClient) {
			super(searchType);
			this.httpTriplestoreClient = httpTriplestoreClient;
		}

		@Override
		protected Model actualExecute() throws ApiException {
			return this.httpTriplestoreClient.search(subjectClass, predicate, objects, objectType, limit, offset);
		}
	}

	private Model search(String subjectClassQname, String predicateQname, Collection<String> objects, String objectType, int limit, int offset) throws ApiException {
		WebTarget webTarget = target().path("search")
				.queryParam("limit", limit)
				.queryParam("offset", offset)
				.queryParam("predicate", predicateQname)
				.queryParam(objectType, toValueParameter(objects));
		if (given(subjectClassQname)) {
			webTarget.queryParam("type", subjectClassQname);
		}
		try {
			Optional<Model> modelForRequest = getModelForRequest(webTarget.request());
			try {
				return modelForRequest.get();
			} catch (IllegalStateException e) {
				// absent optional happens sometimes even though it shouldn't, add some debug information
				String err = String.format("A search query returned an absent value for predicate \"%s\"," +
						" type (%s) values (%s), objectType %s, limit %d, offset %d", subjectClassQname, predicateQname, objects, objectType, limit, offset);
				throw new RuntimeException(err, e);
			}
		} catch (TooManyResultsException e) {
			// this shouldnt happen either with search
			throw new RuntimeException(e);
		}
	}

	private boolean given(String s) {
		return s != null && !s.isEmpty();
	}

	private String toValueParameter(Collection<String> values) {
		StringBuilder objects = new StringBuilder();
		Iterator<String> i = values.iterator();
		while (i.hasNext()) {
			objects.append(i.next());
			if (i.hasNext()) {
				objects.append(",");
			}
		}
		String valueParameter = objects.toString();
		return valueParameter;
	}

	@Override
	public Optional<Model> getResource(String resourceName) throws ApiException {
		try {
			return this.getResource(resourceName, new HashMap<String, String>());
		} catch (TooManyResultsException e) {
			// should not happen with a single resource
			throw new RuntimeException(e);
		}
	}

	@Override
	public Optional<Model> getResource(String resourceName, Map<String, String> params) throws ApiException, TooManyResultsException {
		WebTarget path = target().path(resourceName);

		for (Map.Entry<String, String> entry : params.entrySet()) {
			path = path.queryParam(entry.getKey(), entry.getValue());
		}

		return getModelForRequest(path.request());
	}

	@Override
	public TaxonSearchResult taxonSearch(String query) throws ApiException {
		// use the master checklist
		return taxonSearch(query, "MR.1");
	}

	@Override
	public TaxonSearchResult taxonSearch(String query, String checklist) throws ApiException {
		final int MIN_QUERY_LENGTH = 3;
		if (query.length() < MIN_QUERY_LENGTH) {
			throw new IllegalArgumentException("Search query length must be at least "
					+ MIN_QUERY_LENGTH + " characters");
		}
		if (checklist == null || checklist.length() == 0) {
			throw new IllegalStateException("checklist name must be provided");
		}

		Response response = null;
		try {
			response = target()
					.path("taxon-search")
					.path(query)
					.queryParam("checklist", checklist)
					.request().get();
			raiseForNonSuccess(response);
			Document document = response.readEntity(Document.class);
			return TaxonSearchResultDeserializer.parse(document);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public void updatePredicate(PredicateUpdateOperation operation) throws ApiException {
		for (String lang : operation.value.keySet()) {
			Form form = new Form().param("predicate_qname", operation.predicate)
					.param("objectliteral", operation.value.get(lang))
					.param("langcode", lang);
			if (operation.context != null) {
				form = form.param("context_qname", "LA." + operation.context);
			}

			Response response = null;
			try {
				response = target().path(operation.resourceName).request().post(Entity.form(form));
				raiseForNonSuccess(response);
			} finally {
				if (response != null) {
					response.close();
				}
			}
		}
	}

	@Override
	public void upsertResource(String resourceName, Model model) throws ApiException {
		Response response = null;
		try (StringWriter writer = new StringWriter()) {
			model.write(writer, "RDF/XML-ABBREV");
			String serialized = writer.getBuffer().toString();
			Form form = new Form("data", serialized);
			response = target().path(resourceName).request().post(Entity.form(form));
			raiseForNonSuccess(response);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public String getSequence(String namespace) throws ApiException {
		Response response = null;
		try {
			response = target().path("uri").path(namespace).request().get();
			raiseForNonSuccess(response);
			return response.readEntity(NamespaceEntity.class).response.qname;
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public void delete(String resourceName) throws ApiException {
		Response response = null;
		try {
			response = target().path(resourceName).request().delete();
			raiseForNonSuccess(response);
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}

	@Override
	public Map<String, Resource> getResources(String... resourceNames) throws ApiException {
		return getResources(Arrays.asList(resourceNames));
	}

	@Override
	public Map<String, Resource> getResources(List<String> resourceNames) throws ApiException {
		if (resourceNames.size() == 0) {
			throw new IllegalStateException("at least one resource name must be provided");
		}
		String path = Joiner.on("+").join(resourceNames);
		try {
			Optional<Model> multiModel = getModelForRequest(target().path(path).request());
			return multiModel.isPresent() ? MultiModelParser.parseModel(multiModel.get()) : new HashMap<>();
		} catch (TooManyResultsException e) {
			// should not happen
			throw new RuntimeException(e);
		}
	}

	private Optional<Model> getModelForRequest(Invocation.Builder request) throws ApiException, TooManyResultsException {
		Response response = request.get();
		try {
			// Not found
			if (response.getStatus() == 404) {
				return Optional.absent();
			}
			if (response.getStatus() == 403) {
				throw new TooManyResultsException("Too many results");
			}
			raiseForNonSuccess(response);

			String content = response.readEntity(String.class);
			return Optional.of(ModelFactory.createDefaultModel().read(new StringReader(content), null));
		} finally {
			response.close();
		}
	}

	private void raiseForNonSuccess(Response response) throws ApiException {
		if (!response.getStatusInfo().getFamily().equals(Response.Status.Family.SUCCESSFUL)) {
			ApiException.ApiExceptionBuilder builder = ApiException.builder()
					.source(TriplestoreClient.SOURCE)
					.code(Integer.toString(response.getStatus()));

			try {
				builder = builder.details(response.readEntity(String.class));
			} catch (ProcessingException e) {
				// failed to read error details, just continue..
			}
			throw builder.build();
		}
	}

	private WebTarget target() {
		// lazy webtarget init
		synchronized (this) {
			if (this.target == null) {
				Client client = ClientBuilder.newBuilder()
						.register(new LoggingFeature())
						.build();
				String username = triplestorePreferences.getUsername();
				String password = triplestorePreferences.getPassword();
				this.target = client.target(this.triplestorePreferences.getBaseUri())
						.register(HttpAuthenticationFeature.basic(username, password));
			}
		}

		return target;
	}

	private static class NamespaceEntity {
		public UriResponse response;
	}

	private static class UriResponse {
		public String qname;
		@SuppressWarnings("unused")
		public URI uri;
	}

}