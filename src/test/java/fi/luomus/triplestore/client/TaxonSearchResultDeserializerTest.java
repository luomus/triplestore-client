package fi.luomus.triplestore.client;

import fi.luomus.triplestore.client.model.TaxonSearchMatchWithSimilarity;
import fi.luomus.triplestore.client.model.TaxonSearchResult;
import fi.luomus.triplestore.client.util.TaxonSearchResultDeserializer;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

public class TaxonSearchResultDeserializerTest extends TestCase {
    private DocumentBuilder documentBuilder;

    @Before
    public void setUp() throws ParserConfigurationException {
        documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    }

    @Test
    public void testEmpty() throws Exception {
        Document parsed = documentBuilder.parse(org.apache.commons.io.IOUtils.toInputStream("<results/>"));
        TaxonSearchResult result = TaxonSearchResultDeserializer.parse(parsed);
        assertEquals(0, result.getExactMatch().size() + result.getPartialMatches().size() + result.getLikelyMatches().size());
    }

    @Test
    public void testDuplicate() throws Exception {
        TaxonSearchResult result = TaxonSearchResultDeserializer.parse(parseXML("with_duplicates.xml"));
        assertEquals(2, result.getLikelyMatches().size());
        assertEquals("MX.123", result.getLikelyMatches().get(0).getTaxonId());
        assertEquals("MX.123", result.getLikelyMatches().get(1).getTaxonId());
        assertEquals("foo", result.getLikelyMatches().get(0).getContent());
        assertEquals("bar", result.getLikelyMatches().get(1).getContent());
    }

    @Test
    public void testExact() throws IOException, SAXException {
        TaxonSearchResult result = TaxonSearchResultDeserializer.parse(parseXML("exact.xml"));
        TaxonSearchResult expectedResult = new TaxonSearchResult();
        expectedResult.getExactMatch().add(match());
        assertEquals(expectedResult, result);
    }

    @Test
    public void testLikely() throws IOException, SAXException {
        TaxonSearchResult result = TaxonSearchResultDeserializer.parse(parseXML("likely.xml"));
        TaxonSearchResult expectedResult = new TaxonSearchResult();
        TaxonSearchMatchWithSimilarity match = match();
        match.setSimilarity((float) 0.42);
        expectedResult.getLikelyMatches().add(match);
        assertEquals(expectedResult, result);
    }

    @Test
    public void testPartial() throws IOException, SAXException {
        TaxonSearchResult result = TaxonSearchResultDeserializer.parse(parseXML("partial.xml"));
        TaxonSearchResult expectedResult = new TaxonSearchResult();
        expectedResult.getPartialMatches().add(match());
        assertEquals(expectedResult, result);
    }

    private TaxonSearchMatchWithSimilarity match() {
        TaxonSearchMatchWithSimilarity match = new TaxonSearchMatchWithSimilarity();
        match.setContent("vernacular");
        match.setTaxonId("MX.123");
        match.setScientificName("scientific");
        match.setScientificNameAuthorship("author");
        match.setTaxonRank("MX.rank");

        return match;
    }

    private Document parseXML(String resourceName) throws IOException, SAXException {
        InputStream inputStream = this.getClass().getResource("/" + resourceName).openStream();
        return documentBuilder.parse(inputStream);
    }
}