package fi.luomus.triplestore.client;

import java.net.URL;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

import junit.framework.TestCase;

public class MultiModelParserTest extends TestCase {

	public void testParseModel() throws Exception {
		URL resource = this.getClass().getResource("/multi_get.xml");
		Model multiGet = ModelFactory.createDefaultModel().read(resource.openStream(), null);
		Map<String, Resource> models = MultiModelParser.parseModel(multiGet);
		assertEquals(2, models.size());
		assertTrue(models.containsKey("MX.46604"));
		assertTrue(models.containsKey("MX.37833"));
		assertEquals("Wild Cat", models.get("MX.46604").getProperty(multiGet.createProperty("http://rs.tdwg.org/dwc/terms/", "vernacularName")).getString());
	}

}