package fi.luomus.triplestore.client.util;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URL;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.junit.Test;

import com.google.common.collect.ArrayListMultimap;

import fi.luomus.triplestore.client.model.Constants;

public class ResourceToMapConverterTest {

	@Test
	public void test() throws IOException {
		URL resourceLocation = this.getClass().getResource("/simple_resource.xml");
		Model model = ModelFactory.createDefaultModel().read(resourceLocation.openStream(), null);
		Resource resource = model.getResource(Constants.DEFAULT_NS_URI + "MA.65");
		ArrayListMultimap<String, String> expected = ArrayListMultimap.create();
		expected.put("MA.attrib", "foo");
		expected.put("MA.multi", "baz");
		expected.put("MA.multi", "bar");
		assertEquals(expected, ResourceToMapConverter.getAllPredicates(resource));
	}

}