package fi.luomus.triplestore.client;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Test;

import com.google.common.base.Optional;

import fi.luomus.utils.exceptions.ApiException;

@SuppressWarnings("unchecked")
public class CachedTriplestoreSourceImplTest {
	final String RESOURCE_NAME = "MX.123";

	@Test
	public void testPresent() throws ApiException, TriplestoreClient.TooManyResultsException {
		TriplestoreClient mockClient = mock(TriplestoreClient.class);
		when(mockClient.getResource(anyString(), anyMap())).thenReturn(Optional.<Model>absent());
		CachedTriplestoreClientImpl cachedTriplestoreSource = new CachedTriplestoreClientImpl(60, mockClient);


		Optional<Model> resource = cachedTriplestoreSource.getResource(RESOURCE_NAME);
		assertFalse(resource.isPresent());
	}

	@Test
	public void testNotPresent() throws ApiException, TriplestoreClient.TooManyResultsException {
		TriplestoreClient mockClient = mock(TriplestoreClient.class);
		when(mockClient.getResource(anyString(), anyMap())).thenReturn(Optional.<Model>absent());
		when(mockClient.getResource(eq(RESOURCE_NAME), anyMap())).thenReturn(Optional.of(ModelFactory.createDefaultModel()));
		CachedTriplestoreClientImpl cachedTriplestoreSource = new CachedTriplestoreClientImpl(60, mockClient);

		cachedTriplestoreSource.getResource(RESOURCE_NAME);
		cachedTriplestoreSource.upsertResource(RESOURCE_NAME, ModelFactory.createDefaultModel());
		cachedTriplestoreSource.getResource(RESOURCE_NAME);
		verify(mockClient, times(2)).getResource(eq(RESOURCE_NAME), anyMap());
	}

	@Test
	public void testCacheInvalidation() throws ApiException, TriplestoreClient.TooManyResultsException {
		TriplestoreClient mockClient = mock(TriplestoreClient.class);
		when(mockClient.getResource(anyString(), anyMap())).thenReturn(Optional.<Model>absent());
		when(mockClient.getResource(eq(RESOURCE_NAME), anyMap())).thenReturn(Optional.of(ModelFactory.createDefaultModel()));
		CachedTriplestoreClientImpl cachedTriplestoreSource = new CachedTriplestoreClientImpl(60, mockClient);

		cachedTriplestoreSource.getResource(RESOURCE_NAME);
		cachedTriplestoreSource.upsertResource(RESOURCE_NAME, ModelFactory.createDefaultModel());
		cachedTriplestoreSource.getResource(RESOURCE_NAME);
		verify(mockClient, times(2)).getResource(eq(RESOURCE_NAME), anyMap());
	}

	@Test
	public void testCache() throws TriplestoreClient.TooManyResultsException, ApiException {
		TriplestoreClient mockClient = mock(TriplestoreClient.class);
		when(mockClient.getResource(anyString(), anyMap())).thenReturn(Optional.<Model>absent());
		when(mockClient.getResource(eq(RESOURCE_NAME), anyMap())).thenReturn(Optional.of(ModelFactory.createDefaultModel()));
		CachedTriplestoreClientImpl cachedTriplestoreSource = new CachedTriplestoreClientImpl(60, mockClient);

		cachedTriplestoreSource.getResource(RESOURCE_NAME);
		cachedTriplestoreSource.getResource(RESOURCE_NAME);
		verify(mockClient, times(1)).getResource(eq(RESOURCE_NAME), anyMap());
	}

	@Test
	public void testTooManyResults() throws TriplestoreClient.TooManyResultsException, ApiException {
		TriplestoreClient mock = mock(TriplestoreClient.class);
		when(mock.getResource(eq(RESOURCE_NAME), anyMap())).thenThrow(new TriplestoreClient.TooManyResultsException(""));
		CachedTriplestoreClientImpl cachedTriplestoreSource = new CachedTriplestoreClientImpl(60, mock);
		try {
			cachedTriplestoreSource.getResource(RESOURCE_NAME, new HashMap<String, String>());
			fail("exception not received");
		} catch (ApiException e) {
			throw new RuntimeException(e);
		} catch (TriplestoreClient.TooManyResultsException e) {
			// nop
		}
	}
}
