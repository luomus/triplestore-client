# Triplestore-client

Epävirallinen Java-asiakaskirjasto Triplestoren käpistelyyn. Palauttaa Jenan jäsentämiä ontologiamalleja, joiden jatkojäsentäminen jää 
kirjastoa hyödyntävän järjestelmän harteille.

## Asennus
```
$ git clone ...
$ cd triplestore-client
$ mvn install
```

## Käyttö
```
        new HTTPTriplestoreClientImpl(new TriplestorePreferences() {
            @Override
            public URI getBaseUri() {
                return new URI("https://...");
            }

            @Override
            public String getUsername() {
                return "user";
            }

            @Override
            public String getPassword() {
                return "password";
            }
        });
```
